using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [SerializeField] protected GameObject[] Nodes;

    [SerializeField] protected string Name;

    [SerializeField] protected int TypeID; // 0 for TOGO, 1 for AssetCode, 2 for ActionCode

    protected GameManager GameManagerScript;
    protected GameObject gameManager;




    public void SetTypeID(int value)
    {
        TypeID = value;
    }
    public int GetTypeID()
    {
        return TypeID;
    }

    public void SetName(string Name)
    {
        this.Name = Name;
    }

    public string GetName()
    {
        return name;
    }

    public override bool Equals(object obj)
    {
        return obj is Node node &&
               base.Equals(obj) &&
               TypeID == node.TypeID;
    }
}
