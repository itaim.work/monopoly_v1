using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject[] Nodes;
    [SerializeField] private GameObject[] WayPoints;
    [SerializeField] private string[] Collections;
    [SerializeField] private List<GameObject> Players;
    [SerializeField] private int ActivePlayer;

    private UI_Handler UI_Handler_script;


    // Start is called before the first frame update
    void Start()
    {
        UI_Handler_script = GameObject.Find("Canvas").GetComponent<UI_Handler>();
        Nodes = GameObject.FindGameObjectsWithTag("Node");
        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");
        Array.Sort(WayPoints, CompareByName);
        ActivePlayer = UnityEngine.Random.Range(0, Players.Count);
        UI_Handler_script.SetTurnText("Turn: " + Players[ActivePlayer].GetComponent<PlayerController>().GetName() + ".");
    }


    // Update is called once per frame
    void Update()
    {

    }

    public GameObject[] getNodes()
    {
        return Nodes;
    }

    public int calcTrainCount(int ownerID, AssetCode CodeToTest)
    {

        int count = 0;


        for (int i = 0; i < this.Nodes.Length; i++)
        {
            if (CodeToTest.GetTypeID() == this.Nodes[i].GetComponent<AssetCode>().GetTypeID())
            {
                count++;
            }
        }
        return count;


    }

    private int CompareByName(GameObject A, GameObject B)
    {
        return A.transform.name.CompareTo(B.transform.name);
    }

    public PlayerController GetActivePlayerController()
    {
        return Players[ActivePlayer].GetComponent<PlayerController>();
    }

    public string[] getCollections()
    {
        return this.Collections;
    }

    public List<GameObject> GetPlayers()
    {
        return this.Players;
    }

    public void TransectMoneyBetweenPlayers(int source, int dest, int amount)
    {
        Players[dest].GetComponent<PlayerController>().SetMoney(Players[dest].GetComponent<PlayerController>().GetMoney() - amount);
        Players[source].GetComponent<PlayerController>().SetMoney(Players[source].GetComponent<PlayerController>().GetMoney() + amount);

    }

    public void Roll()
    {
        int value = 0, x, y;
        x = UnityEngine.Random.Range(1, 7);
        value += x;
        y = UnityEngine.Random.Range(1, 7);
        value += y;
        UI_Handler_script.SetCubeDisplay(x + " , " + y + " , " + value);
        Players[ActivePlayer].GetComponent<PlayerMovement>().Move(value);
        UI_Handler_script.SetEndTurnButtonState(true);
        UI_Handler_script.SetMoveButtonState(false);
    }

    public void EndTurn()
    {
        UI_Handler_script.SetMoveButtonState(true);

        UI_Handler_script.SetEndTurnButtonState(false);

        do
        {
            ActivePlayer++;
            if (ActivePlayer == Players.Count)
            {
                ActivePlayer = 0;
            }
        } while (Players[ActivePlayer].GetComponent<PlayerController>().GetMoney() <= 0);

        UI_Handler_script.Reset_UI();
        UI_Handler_script.SetTurnText("Turn: " + Players[ActivePlayer].GetComponent<PlayerController>().GetName() + ".");

    }

    public int GetActivePlayerID()
    {
        return this.ActivePlayer;
    }
}

