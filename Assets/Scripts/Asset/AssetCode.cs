using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AssetCode : Node
{

    [SerializeField] protected int price, MortgagePrice;

    [SerializeField] protected int OwnerID = -1;
    [SerializeField] protected bool IsMortgaged;

    private void Start()
    {
        this.SetTypeID(1);
    }

    public int GetOwnerID()
    {
        return OwnerID;
    }

    public int GetPrice()
    {
        return this.price;
    }

    public void SetOwnerID(int value)
    {
        this.OwnerID = value;
    }

    public virtual int GetMortgagePrice()
    {
        return MortgagePrice;
    }

    public bool getIsMortgaged()
    {
        return this.IsMortgaged;
    }
    public void SetIsMortgaged(bool value)
    {
        this.IsMortgaged = value;
    }

    public abstract int calcFee();

    public virtual void ResetAsset()
    {
        IsMortgaged = false;
        OwnerID = -1;
    }
}
