using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetCodeProperty : AssetCode
{
    [SerializeField] protected int HousePrice, CollectionID, HouseNum, fee;
    [SerializeField] protected bool IsCollectionComplete;
    [SerializeField] private GameObject[] Houses;

    public override int calcFee()
    {
        if (IsCollectionComplete)
        {
            if (HouseNum > 0)
            {
                return HouseNum * HousePrice;
            }
            return fee * 2;
        }
        return fee;
    }
    public int GetCollectionID()
    {
        return this.CollectionID;
    }

    public void buyHouse()
    {
        Houses[HouseNum++].SetActive(true);
    }

    public override int GetMortgagePrice()
    {
        return base.GetMortgagePrice()  + HouseNum * (HousePrice / 2);
    }

    public override void ResetAsset()
    {
        base.ResetAsset();
        IsCollectionComplete = false;
        HouseNum = 0;
        for (int i = 0; i < Houses.Length; i++)
        {
            Houses[i].SetActive(false);
        }
        {

        }
    }
}
