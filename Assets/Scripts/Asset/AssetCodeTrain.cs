using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetCodeTrain : AssetCode
{


    private void Start()
    {
        gameManager = GameObject.Find("GameManager");
        GameManagerScript = gameManager.GetComponent<GameManager>();

        this.SetTypeID(4);

        Invoke("test", 2);
    }

    private void Update()
    {
    }

    public void test()
    {

        Nodes = GameManagerScript.getNodes();
    }

    

    
    public override int calcFee()
    {
        return 25 * GameManagerScript.calcTrainCount(this.GetOwnerID(), transform.GetComponent<AssetCodeTrain>());
    }

}
