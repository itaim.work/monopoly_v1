using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Handler : MonoBehaviour
{
    [SerializeField]
    private GameObject buyButton, NoMoneyText, AssetDisplayProperty, RollButton, EndTurnButton,
    TurnStateText, MortgageButton, PayMortgageButton, MoneyDisplay, CubeDisplay, NoMoneyEndText, EndGamePerPlayerButton;

    [SerializeField] private Dropdown AssetDropDown;

    [SerializeField] private GameManager GameManagerScript;

    private void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        AssetDropDown.onValueChanged.AddListener(delegate
        {
            displayDropDownAsset();
        });
    }

    private void Update()
    {
        MoneyDisplay.GetComponent<Text>().text = "Money Amount: " + GameManagerScript.GetActivePlayerController().GetMoney();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManagerScript.GetActivePlayerController().EndTurnCheck();
        }
    }

    public void SetCubeDisplay(string value)
    {
        CubeDisplay.GetComponent<Text>().text = value;
    }
    public void SetEndGamePerPlayerButton(bool value)
    {
        this.EndGamePerPlayerButton.SetActive(value);
    }
    public void displayDropDownAsset()
    {
        Reset_UI();
        AssetDropDown.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssets())
        {
            if (item.GetName().Equals(AssetDropDown.options[AssetDropDown.value].text))
            {
                if (item.gameObject.CompareTag("Asset"))
                {
                    DisplayProperty((AssetCodeProperty)item);
                }
                else if (item.gameObject.CompareTag("Train"))
                {
                    //display train
                }
                else
                {
                    //display company
                }

                if (item.getIsMortgaged())
                {
                    if (GameManagerScript.GetActivePlayerController().GetMoney() > item.GetMortgagePrice())
                    {
                        PayMortgageButton.SetActive(true);
                    }
                }
                else
                {
                    MortgageButton.SetActive(true);
                }
                break;
            }
        }
    }
    public void enablePurchase()
    {
        buyButton.SetActive(true);
    }
    public void SetAssetDropDown(bool value)
    {
        AssetDropDown.gameObject.SetActive(value);
    }
    public void buy()
    {
        GameManagerScript.GetActivePlayerController().BuyAsset();
        buyButton.SetActive(false);
        AssetCode item = GameManagerScript.GetActivePlayerController().GetActiveAsset();
        if (item.gameObject.CompareTag("Asset"))
        {
            DisplayProperty((AssetCodeProperty)item);
        }
        else if (item.gameObject.CompareTag("Train"))
        {
            //display train
        }
        else
        {
            //display company
        }
        MortgageButton.SetActive(true);
    }
    public void UnablePurchase()
    {
        NoMoneyText.SetActive(true);
    }
    public void DisplayProperty(AssetCodeProperty property)
    {
        AssetDisplayProperty.SetActive(true);
        Text temp;
        temp = GameObject.Find("Name_Text").GetComponent<Text>();
        temp.text = "Name: " + property.GetName();
        temp = GameObject.Find("Collection_Text").GetComponent<Text>();
        temp.text = "Collection: " + GameManagerScript.getCollections()[property.GetCollectionID()];
        temp = GameObject.Find("Is_Owned_Text").GetComponent<Text>();
        if (property.GetOwnerID() != -1)
        {
            temp.text = "Owner: " + GameManagerScript.GetPlayers()[property.GetOwnerID()].GetComponent<PlayerController>().GetName();
            temp = GameObject.Find("Price_Text").GetComponent<Text>();
            temp.text = "";

        }
        else
        {
            temp.text = "Not owned";
            temp = GameObject.Find("Price_Text").GetComponent<Text>();
            temp.text = "Price: " + property.GetPrice();
        }
        temp = GameObject.Find("Fee_Text").GetComponent<Text>();
        temp.text = "Fee Amount: " + property.calcFee();
    }
    public void SetEndTurnButtonState(bool value)
    {
        EndTurnButton.SetActive(value);
    }
    public void SetMoveButtonState(bool value)
    {
        RollButton.SetActive(value);
    }
    public void SetTurnText(string text)
    {
        TurnStateText.GetComponent<Text>().text = text;
        MoneyDisplay.GetComponent<Text>().text = "Money Amount: " + GameManagerScript.GetActivePlayerController().GetMoney();
    }
    public void Reset_UI()
    {
        AssetDisplayProperty.SetActive(false);
        NoMoneyText.SetActive(false);
        buyButton.SetActive(false);
        AssetDropDown.gameObject.SetActive(false);
        MortgageButton.SetActive(false);
        PayMortgageButton.SetActive(false);
    }
    public void FillOwnedAssetDropDown(List<AssetCode> assetCodes)
    {
        SetAssetDropDown(true);
        PopulateDropdown(AssetDropDown, assetCodes);
    }
    public void PopulateDropdown(Dropdown dropdown, List<AssetCode> assetCodes)
    {
        List<string> Options = new List<string>();
        foreach (AssetCode item in assetCodes)
        {
            if (item.gameObject.CompareTag("Asset"))
            {
                AssetCodeProperty property = (AssetCodeProperty)item;
                Options.Add(property.GetName() + ", Price: " + property.GetMortgagePrice());
            }
            else
            {
                Options.Add(item.GetName() + ", Price: " + item.GetMortgagePrice());
            }
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(Options);
    }
    public void PayMortgage()
    {
        Reset_UI();
        AssetDropDown.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssets())
        {
            if (item.GetName().Equals(AssetDropDown.options[AssetDropDown.value].text))
            {
                item.SetIsMortgaged(false);
                GameManagerScript.GetActivePlayerController().SetMoney(GameManagerScript.GetActivePlayerController().GetMoney() - item.GetMortgagePrice());

                if (item.gameObject.CompareTag("Asset"))
                {
                    DisplayProperty((AssetCodeProperty)item);
                }
                else if (item.gameObject.CompareTag("Train"))
                {
                    //display train
                }
                else
                {
                    //display company
                }
            }
        }
        PayMortgageButton.SetActive(false);
    }
    public void MortgageAsset()
    {
        string name;
        AssetDropDown.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssets())
        {
            if (item.gameObject.CompareTag("Asset"))
            {
                AssetCodeProperty property = (AssetCodeProperty)item;
                name = property.GetName() + ", Price: " + property.GetMortgagePrice();
            }
            else
            {
                name = item.GetName() + ", Price: " + item.GetMortgagePrice();
            }
            if (name.Equals(AssetDropDown.options[AssetDropDown.value].text))
            {
                item.SetIsMortgaged(true);
                GameManagerScript.GetActivePlayerController().SetMoney(GameManagerScript.GetActivePlayerController().GetMoney() + item.GetMortgagePrice());
                if (item.gameObject.CompareTag("Asset"))
                {
                    DisplayProperty((AssetCodeProperty)item);
                }
                else if (item.gameObject.CompareTag("Train"))
                {
                    //display train
                }
                else
                {
                    //display company
                }


            }
        }
        MortgageButton.SetActive(false);
        GameManagerScript.GetActivePlayerController().EndTurnCheck();
    }
    public void SetNoMoneyEndText(bool value)
    {
        NoMoneyEndText.SetActive(value);
    }
    public void EndGamePerPlayer()
    {
        GameManagerScript.GetActivePlayerController().EndGame();
    }
}
