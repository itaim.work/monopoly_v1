using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private UI_Handler UI_Handler_script;


    [SerializeField] private AssetCode activeAsset;

    [SerializeField] private int PlayerID;
    [SerializeField] private string PlayerName;
    [SerializeField] private int money = 5000;
    [SerializeField] private PlayerMovement PlayerMovementScript;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private List<AssetCode> OwnedAssets;

    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        PlayerMovementScript = gameObject.GetComponent<PlayerMovement>();
        UI_Handler_script = GameObject.Find("Canvas").GetComponent<UI_Handler>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public List<AssetCode> GetOwnedAssets()
    {
        return this.OwnedAssets;
    }

    private void OnTriggerEnter(Collider other)
    {

        UI_Handler_script.Reset_UI();
        if (!other.CompareTag("Player") && GameManagerScript.GetActivePlayerID() == this.PlayerID)
        {
            if (PlayerMovementScript.GetDist() <= 3f)
            {
                if (other.gameObject.CompareTag("Asset"))
                {
                    UI_Handler_script.DisplayProperty(other.gameObject.GetComponent<AssetCodeProperty>());
                    TestAsset(other.gameObject.GetComponent<AssetCode>());
                }
                else if (other.gameObject.CompareTag("ToGo"))
                {
                    print("TOGO");
                    PlayerMovementScript.SetCurrentPos(0);
                }
                else if (other.gameObject.CompareTag("Surprise"))
                {
                    print("Land on Surprise");
                    other.GetComponent<SurpriseNode>().CallFunction();

                }
                else if (other.gameObject.CompareTag("LuckyChest"))
                {
                    print("Land on LuckyChest");
                    other.GetComponent<LuckyChest>().CallFunction();
                }
                else if (other.gameObject.CompareTag("Cop"))
                {
                    print("Land on Cop");
                    PlayerMovementScript.SetCurrentPos(10);

                }
                else if (other.gameObject.CompareTag("Taxes"))
                {
                    print("Land on taxes, pay " + other.gameObject.GetComponent<Taxes>().GetAmountToPay());
                    this.money -= other.gameObject.GetComponent<Taxes>().GetAmountToPay();
                }
                else if (other.gameObject.CompareTag("Train"))
                {
                    //display train
                    TestAsset(other.gameObject.GetComponent<AssetCode>());
                }
                else if (other.gameObject.CompareTag("Company"))
                {
                    //display
                    TestAsset(other.gameObject.GetComponent<AssetCode>());
                }
                if (OwnedAssets.Count > 0)
                {
                    UI_Handler_script.FillOwnedAssetDropDown(OwnedAssets);
                }
                EndTurnCheck();
            }
        }
    }

    public void EndTurnCheck()
    {
        bool flag = false;
        if (money > 0)
        {
            UI_Handler_script.SetEndTurnButtonState(true);
        }
        else
        {


            for (int i = 0; i < OwnedAssets.Count; i++)
            {
                if (!OwnedAssets[i].getIsMortgaged())
                {
                    UI_Handler_script.SetNoMoneyEndText(true);
                    flag = true;
                }

            }

            if (!flag)
            {
                UI_Handler_script.SetEndGamePerPlayerButton(true);
            }
        }

    }

    public void EndGame()
    {
        for (int i = 0; i < OwnedAssets.Count; i++)
        {
            if (OwnedAssets[i].gameObject.CompareTag("Asset"))
            {
                AssetCodeProperty property = (AssetCodeProperty)OwnedAssets[i];
                property.ResetAsset();
            }
            else
            {
                OwnedAssets[i].ResetAsset();
            }
        }
        GameManagerScript.EndTurn();
        UI_Handler_script.SetEndGamePerPlayerButton(false);
    }

    public AssetCode GetActiveAsset()
    {
        return this.activeAsset;
    }
    public void TestAsset(AssetCode asset)
    {
        activeAsset = asset;
        if (asset.GetOwnerID() == -1) // asset free
        {
            if (asset.GetPrice() <= this.money)
            {
                UI_Handler_script.enablePurchase();
            }
            else
            {
                UI_Handler_script.UnablePurchase();
            }
        }
        else
        {
            if (asset.GetOwnerID() == this.PlayerID)
            {
                //build house
            }
            else if (!asset.getIsMortgaged())
            {
                GameManagerScript.TransectMoneyBetweenPlayers(asset.GetOwnerID(), PlayerID, asset.calcFee());
            }
        }
        EndTurnCheck();

    }
    public void BuyAsset()
    {
        this.money -= activeAsset.GetPrice();
        activeAsset.SetOwnerID(this.PlayerID);
        OwnedAssets.Add(activeAsset);
        UI_Handler_script.FillOwnedAssetDropDown(this.OwnedAssets);
    }
    public void SetMoney(int value)
    {
        this.money = value;
    }
    public int GetMoney()
    {
        return this.money;
    }
    public string GetName()
    {
        return this.PlayerName;
    }

}
