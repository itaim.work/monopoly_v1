using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private NavMeshAgent Agent;
    [SerializeField] private GameObject[] WayPoints;
    [SerializeField] private int CurrentPositionInArray;
    [SerializeField] private float Dist;


    // Start is called before the first frame update
    void Start()
    {
        CurrentPositionInArray = 0;
        Agent = gameObject.GetComponent<NavMeshAgent>();
        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

        Array.Sort(WayPoints, SortByName);
    }

    // Update is called once per frame
    void Update()
    {

        Agent.SetDestination(WayPoints[CurrentPositionInArray].transform.position);
        Dist = Vector3.Distance(transform.position, Agent.destination);
    }

    public float GetDist()
    {
        return Dist;
    }

    public void Move(int value)
    {
        CurrentPositionInArray += value;
        while (CurrentPositionInArray >= WayPoints.Length)
        {
            CurrentPositionInArray = CurrentPositionInArray - WayPoints.Length;
        }

    }

    public int SortByName(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }
    public void SetCurrentPos(int value)
    {
        CurrentPositionInArray = value;
    }
}
