using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyChest : ActionNode
{
    // Start is called before the first frame update
    void Start()
    {
        SetStart();
    }

    // Update is called once per frame
    void Update()
    {
        CallCard();
    }
}
