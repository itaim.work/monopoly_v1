using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ActionNode : Node
{
    [SerializeField] protected List<Action> PlayerFunctions;
    [SerializeField] protected UI_Handler UI_Handler_Script;
    [SerializeField] protected GameObject Player;
    [SerializeField] protected bool CanCallFunc;
    [SerializeField] protected int IndexInFuncList;

    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }

    public List<Action> Shuffle(List<Action> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            Action value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
        return list;
    }

    protected void SetStart()
    {
        UI_Handler_Script = GameObject.Find("Canvas").GetComponent<UI_Handler>();
        Player = GameObject.Find("Player");
        PlayerFunctions = new List<Action>();
        PlayerFunctions = Shuffle(PlayerFunctions);

    }

    public void TransectMoneyFromPlayerToBank(GameObject Source, int amount)
    {
        print("player payed " + amount);
        if (Source.CompareTag("Player"))
        {
            Source.GetComponent<PlayerController>().SetMoney(Source.GetComponent<PlayerController>().GetMoney() - amount);
        }
    }
    public void TransectMoneyFromBankToPlayer(GameObject Target, int amount)
    {
        print("player Won " + amount);
        if (Target.CompareTag("Player"))
        {
            Target.GetComponent<PlayerController>().SetMoney(Target.GetComponent<PlayerController>().GetMoney() + amount);
        }
    }
    public void MoveToPos(GameObject user, int DestIndex, String DestName)
    {
        if (user.CompareTag("Player"))
        {
            user.GetComponent<PlayerMovement>().SetCurrentPos(DestIndex);
        }
    }

    protected void CallCard()
    {
        if (CanCallFunc)
        {
            PlayerFunctions[IndexInFuncList++].Invoke();

            if (IndexInFuncList == PlayerFunctions.Count)
            {
                IndexInFuncList = 0;
            }
            CanCallFunc = false;
        }

    }
    public void CallFunction()
    {
        CanCallFunc = true;
    }

}
