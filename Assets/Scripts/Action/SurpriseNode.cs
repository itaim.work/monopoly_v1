using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SurpriseNode : ActionNode
{
    // Start is called before the first frame update
    private void Start()
    {
        SetStart();
        PlayerFunctions.Add(() => TransectMoneyFromBankToPlayer(Player, 50));
        PlayerFunctions.Add(() => MoveToPos(Player, 0, "TO GO"));
    }

    // Update is called once per frame
    private void Update()
    {
        CallCard();
    }



}
